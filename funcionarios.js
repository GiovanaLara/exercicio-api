const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8080;

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/funcionarios',(req,res)=>{
    res.send("{message: funcionario encontrado}");
});

app.get('/funcionarios/pesquisa/:valor',(req, res)=>{
    console.log("Entrou");
    let dado = req.params.valor;
    let ret = "Dados solicitado:" + dado;
    res.send("{message:"+ret+"}");
});


app.post('/funcionarios',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);

    if(headers_ != headers_ || headers_ == null){
        console.log("Valor Inválido no Header: " + headers_)
        let resp = "Valor no header inválido: " + headers_
        res.send(resp) 
    };

    let ret = "Dados enviados com sucesso: Nome:" + dados.nome;
    ret+=" Sobrenome: " + dados.sobrenome;
    ret+=" RACF: " + dados.racf;
    res.send("{message:"+ret+"}");
});

app.put('/funcionarios',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);

    if(headers_ != headers_ || headers_ == null){
        console.log("Valor Inválido no Header: " + headers_)
        let resp = "Valor no header inválido: " + headers_;
        res.send(resp) 
    };

    let ret = "Dados enviados: Nome:" + dados.nome;
    ret+=" Sobrenome: " + dados.sobrenome;
    res.send("{message:"+ret+"}");
});

app.delete('/funcionarios/:valor',(req, res)=>{
    let dados = req.body;
    let headers_ = req.headers["access"];
    console.log("Valor Access: " + headers_);

    if(headers_ != headers_ || headers_ == null){
        console.log("Valor Inválido no Header: " + headers_)
        let resp = "Valor no header inválido: " + headers_
        res.send(resp) 
    };

    let ret = "Dados deletados: Nome:" + dados.nome;
    ret+=" Sobrenome: " + dados.sobrenome;
    res.send("{message:"+ret+"}");
});